import React, { Component } from 'react';

class AuthForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      username: "",
      password: "",
      profileImageUrl: ""
    }
  }
  
  handleChange = e => {
    this.setState({
      // Use a "computed propery name" to make this function generic 
      [e.target.name]: e.target.value
    });
  }
  
  handleSubmit = e => {
    e.preventDefault();
    const authType = this.props.signUp ? "signup" : "signin";
    this.props.onAuth(authType, this.state).then(() => {
      console.log("LOGGED IN!");
      // Use the react router `history` object to change the 
      //  current URL and go back to the homepag
      this.props.history.push("/");
    }).catch(() => {
      return;
    });
  };
  
  render() {
    const { email, username, password, profileImageUrl } = this.state;
    // `history` comes from the React Router
    const { heading, buttonText, signUp, errors, history, removeError } = this.props;
    
    // Anytime there is a change in the route via the React Router histroy object,
    // //  remove the current error message for the user
    // history.listen(()=> {
    //   removeError();
    // });
    // **More advanced method-
    if (errors.message) {
      const unlisten = history.listen(() => {
        removeError()
        unlisten()
      });
    }
    
    return(
      <div>
        <div className="row justify-content-md-center text-center">
        <div className="col-md-6">
          <form onSubmit={this.handleSubmit}>
            <h2>{heading}</h2>
            {errors.message && <div className="alert alert-danger">{errors.message}</div>}
            <label htmlFor="email">Email: </label>
            <input 
              className="form-control" 
              id="email" 
              name="email" 
              onChange={this.handleChange} 
              value={email} 
              type="text"
            />
            <label htmlFor="password">Password:</label>
            <input
              className="form-control"
              id="password"
              name="password"
              onChange={this.handleChange}
              value={password}
              type="password"
            />
            {signUp && (
                <div>
                  <label htmlFor="username">Username: </label>
                  <input 
                    className="form-control" 
                    id="username" 
                    name="username" 
                    onChange={this.handleChange} 
                    value={username} 
                    type="text"
                  />
                  <label htmlFor="image-url">Image URL:</label>
                  <input
                    className="form-control"
                    id="image-url"
                    name="profileImageUrl"
                    onChange={this.handleChange}
                    value={profileImageUrl}
                    type="text"
                  />
                </div>
              )
            }
            <button 
              type="submit" 
              className="btn btn-primary btn-block btn-lg"
            >
              {buttonText}
            </button>
          </form>
        </div>
        </div>
      </div>
    );
  }
}

export default AuthForm;