import rootReducer from "./reducers";
// Middleware is important so we can use 'redux-thunk' for async calls 
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";


// Create the store by passing in the rootReducer and setting up redux-thunk
//  and redux chrome dev-tools
export function configureStore() {
  const store = createStore(
    rootReducer,
    compose(
      applyMiddleware(thunk),
      // **Add this in to use Redux DevTools
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
  );
  
  return store;
}