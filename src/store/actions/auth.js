import { apiCall, setTokenHeader } from "../../services/api";
import { SET_CURRENT_USER } from "../actionTypes";
import { addError, removeError } from "./errors"; 

export function setCurrentUser(user) {
  return {
    type: SET_CURRENT_USER,
    user
  };
}

// This function comes `from services/api.js`, it allows us to add/remove the JWT token
//  from the HTTP header for access to the api.
export function setAuthorizationToken(token) {
  setTokenHeader(token);
}

export function logout() {
  return dispatch => {
    localStorage.clear();
    setAuthorizationToken(false);
    dispatch(setCurrentUser({}));
  }
}

export function authUser(type, userData) {
  return dispatch => {
    // wrap our thunk in a promise so we can wait for the API call
    return new Promise((resolve, reject) => {
      return apiCall("post", `/api/auth/${type}`, userData)
        .then(({ token, ...user }) => {
          localStorage.setItem("jwtToken", token);
          setAuthorizationToken(token);
          dispatch(setCurrentUser(user));
          dispatch(removeError()); // Remove the current error 
          resolve(); // indicate that the API call succeeded
        })
        .catch(err => {
          dispatch(addError(err.message)); // Add the error to the state  
          reject(); // indicate the API call failed
        });
    });
  };
}
