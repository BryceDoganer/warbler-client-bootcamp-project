import { SET_CURRENT_USER } from "../actionTypes";

// `user` is an object stored in the api. 
// It has the following properties: 
//  email: String *required
//  username: String *required
//  password: String *required 
//  profileImageUrl: String
//  messages: Array of `message` objects
const DEFAULT_STATE = {
  isAuthenticated: false, // hopefully is true when logged in
  user: {} // All the user information when they are logged in
};

export default (state = DEFAULT_STATE, action) => {
  switch(action.type) {
    case SET_CURRENT_USER:
      return {
        // Check to see if there is a user logged in
        // Object.keys() returns an array of "keys" in the given object, if it's empty
        //  then the length of the array of keys is 0.
        isAuthenticated: !!Object.keys(action.user).length,
        user: action.user
      };
    default:
      return state;
  }
}