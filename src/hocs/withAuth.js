import React, { Component } from 'react';
import { connect } from 'react-redux';

// This is a "Higher Order Component" (hoc)
//  The component to be rendered is passed in, but before rendering it, this component
//  checks if the current user is authenticated. If they are not it redirects to the
//  `signin` route
export default function withAuth(ComponentToBeRendered) {
  class Authenticate extends Component {
    componentWillMount() {
      if(!this.props.isAuthenticated) {
        this.props.history.push("/signin");
      }
    }
    
    componentWillUpdate(nextProps) {
      if(!nextProps.isAuthenticated) {
        this.props.history.push("/signin");
      }
    }

    render() {
      return <ComponentToBeRendered {...this.props} />;
    }
  }
  
  function mapStateToProps(state) {
    return {
      isAuthenticated: state.currentUser.isAuthenticated
    };
  }
  
  return connect(mapStateToProps)(Authenticate);
}
