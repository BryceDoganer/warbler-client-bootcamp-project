import React from 'react';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Homepage from '../components/Homepage';
import AuthForm from '../components/AuthForm';
import { authUser } from '../store/actions/auth'; // Include in `mapDispatchToProps` below 
import { removeError } from '../store/actions/errors'; // Include in `mapDispatchToProps` below
import MessageForm from '../containers/MessageForm';
import withAuth from '../hocs/withAuth';

const Main = props => {
  const { authUser, errors, removeError, currentUser } = props;
  return (
    <div className="container">
      <Switch>
        <Route 
          exact
          path="/" 
          render={props => <Homepage currentUser={currentUser} {...props} />} 
        />
        <Route
          exact
          path="/signin"
          render={props => {
            return (
              <AuthForm
                removeError={removeError}
                errors={errors}
                onAuth={authUser}
                buttonText="Log in"
                heading="Welcome Back."
                {...props}
              />
            );
          }}
        />
        <Route
          exact
          path="/signup"
          render={props => {
            return (
              <AuthForm
                removeError={removeError}
                errors={errors}
                onAuth={authUser}
                signUp
                buttonText="Sign me up!"
                heading="Join Warbler today."
                {...props}
              />
            );
          }}
        />
        <Route
          path="/users/:id/messages/new"
          component={withAuth(MessageForm)}
        />
      </Switch>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser,
    errors: state.errors
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators (
    {
      authUser,
      removeError
    },
    dispatch
  );
}

// The two parameters in connect are `mapStateToProps` and `mapDispatchToProps`
//  `authUser` is an action and must be mapped to props
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));