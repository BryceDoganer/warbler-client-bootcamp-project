import React from 'react';
import { Provider } from 'react-redux';
import { configureStore } from '../store';
import { BrowserRouter as Router } from 'react-router-dom';
import Navbar from './NavBar.js';
import Main from './Main.js';
import { setAuthorizationToken, setCurrentUser } from "../store/actions/auth";
import jwtDecode from "jwt-decode";

// We pass in the store from our helper function which combines the reducers and
//  sets up redux thunk for async calls
const store = configureStore();

// This is the concept of "hydration". If a user is disconnected, this will check if there is
//  a JWT token in localStorage and go ahead and add it to the HTTP header for convenience. 
if(localStorage.jwtToken) {
  setAuthorizationToken(localStorage.jwtToken);
  // Prevent someone from manually tampering with the key of jwtToken in localStorage
  try {
    store.dispatch(setCurrentUser(jwtDecode(localStorage.jwtToken)));
  } catch(err) {
    // If invalid, logout the user 
    store.dispatch(setCurrentUser({}));
  }
}

// Wrap our app in a `Provider` and `Router` component to use redux and the react-router 
//  respectively
// Make sure to pass in our store to the Provider component
const App = () => (
  <Provider store={store}>
    <Router>
      <div className="onboarding">
        <Navbar />
        <Main />
      </div>
    </Router>
  </Provider>
);


export default App;
